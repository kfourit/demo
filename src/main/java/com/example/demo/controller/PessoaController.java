package com.example.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.Pessoa;
import com.example.demo.repository2.PessoaRepository;

@RestController
@RequestMapping(path = "/pessoa", produces = MediaType.TEXT_PLAIN_VALUE)
public class PessoaController {

	@Autowired
	PessoaRepository pessoaRepository;

	@PostMapping
	public void insert(@RequestBody final Pessoa pessoa) {
		pessoaRepository.save(pessoa);
	}

	@GetMapping
	public String retrieve() {
		Pessoa pessoa = new Pessoa();
		pessoa.setId(1);
		pessoa.setNome("joao");
		pessoaRepository.save(pessoa);
		return pessoaRepository.findById(1L).toString();
		//return "{\"body\":\"deu certo\"}";
	}
}
