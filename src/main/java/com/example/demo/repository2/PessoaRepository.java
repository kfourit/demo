package com.example.demo.repository2;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Pessoa;

public interface PessoaRepository extends JpaRepository<Pessoa, Long> {

}
